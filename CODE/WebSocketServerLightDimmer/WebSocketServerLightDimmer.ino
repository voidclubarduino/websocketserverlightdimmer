/*
  Ligth Dimmer via Websocket
  criado a 28 Abril 2016
  por Ivo Oliveira e Bruno Horta
 */
/**
 * INCLUIR A Biblioteca do ESP e do WebsockerServer 
 */
#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>

#define LED_PIN 2
#define WS_PORT 81
#define PHOTO_RESISTOR_PIN A0

int serialCount = 0;
/**
 * Configuração do WI-FI
 */
const char* ssid = "TESLA";
const char* password = "xptoxpto";

/**
 * Instanciar Websocket com uma porta especifica
 */
WebSocketsServer webSocket = WebSocketsServer(WS_PORT);

void setup() {
  //Iniciar a porta Serie para Debug
  Serial.begin(115200);
  //pausa para proteger de overhead
  delay(10);
  
  pinMode(PHOTO_RESISTOR_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  //Ativar apenas modo estação
  WiFi.mode(WIFI_STA);
  //Iniciar o Modulo Wif-Fi
  WiFi.begin(ssid, password);
  //Aguarda até estar Ligado
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  //DEBUG
  Serial.println("");
  Serial.print("WiFi ligado a: ");
  Serial.println(ssid);

  /*
   * Inicia o Websocket
   */
  webSocket.begin();
  //Regista o Handler para tratar dos eventos do Socket
  webSocket.onEvent(webSocketEvent);
  //DEBUG
  Serial.println("WebSocket Iniciado");

  /* 
   * Imprime o endereço IP do Modulo
   */
  Serial.println(WiFi.localIP());
}

void loop() {
  //Escuta Eventos
  webSocket.loop();
  //Processa Sensor
  processPhotoResistorData();
}

/**
 * Processa os Eventos do Socket
 */
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch(type) {
    case WStype_DISCONNECTED:
        Serial.printf("[%u] Desligado!\n", num);
        break;
    case WStype_CONNECTED:
        {
            IPAddress ip = webSocket.remoteIP(num);
            Serial.printf("[%u] Ligado  %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
            // Envia a mensagem para o cliente
            webSocket.sendTXT(num, "{\"status\":\"Ligado\"}");
        }
        break;
    case WStype_TEXT:
    //não implementado neste exemplo
        break;
    case WStype_BIN:
    //não implementado neste exemplo
        break;
  }
}

//Processa a leitura do Sensor de luz
void processPhotoResistorData(){
    //pausa para proteger de overhead
    delay(50);
    /**
     * Ler o valor recebido no pino A1 (leitura do sensor)
     */
    int leitura_sensor = analogRead(PHOTO_RESISTOR_PIN); 
    /**
     * Transposição dos valores recebidos, da gama enviada pelo sensor (0-1024)
     * para uma outra gama, aceite como input pelo LED (0-255)
     */
    //Map - Transforma os valores lidos (0 - 1023) noutra gama de valores (0 - 255)
    //0 a 255 são os valores aceites pelo LED
    float valor_led = map(leitura_sensor, 0, 1024, 0, 255);
    /**
     * Envio do valor calculado para o LED
     */
    analogWrite(LED_PIN, valor_led);
    webSocket.broadcastTXT("{\"state\":\"" + String(Light(leitura_sensor)) + "\"}");
   
}

//Conversão da leitura ADC para LUX
double Light (int RawADC0){
  double Vout=RawADC0*0.00322265625;//Leitura_ADC*(VOLTAGEM_REF*1024)
  int lux=(1650/Vout-500)/10;//(500*VOLTAGEM_REF/Vout-500)/RESISTOR
  return lux;
}
